// (c) 2022 Laurent Ongaro - Gameamea Studio

using UnrealBuildTool;

public class ShooterTarget : TargetRules {
	public ShooterTarget(TargetInfo Target) : base(Target) {
		Type = TargetType.Game;
		DefaultBuildSettings = BuildSettingsVersion.V2;
		ExtraModuleNames.AddRange(new string[] { "Shooter" });
	}
}