// (c) 2022 Laurent Ongaro - Gameamea Studio

#pragma once

#include "Animation/AnimInstance.h"
#include "CoreMinimal.h"

#include "ShooterAnimInstance.generated.h"

/**
 *
 */
UCLASS()
class SHOOTER_API UShooterAnimInstance : public UAnimInstance {
	GENERATED_BODY()
public:
	// public properties
	//////

	// public methods
	//////

	/** Update animation properties */
	UFUNCTION(BlueprintCallable)
	void UpdateAnimationProperties(const float DeltaTime);

	// public overriden methods
	//////

	/** todo comment */
	virtual void NativeInitializeAnimation() override;

protected:
	// protected properties
	//////

	// protected methods
	//////

private:
	// private properties
	//////

	/** The player character to play anims with */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Movement", meta = (AllowPrivateAccess = "true"))
	class AShooterCharacter* ShooterCharacter;

	/** The speed of the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Movement", meta = (AllowPrivateAccess = "true"))
	float Speed;

	/** Whether or not the character is in the air */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Movement", meta = (AllowPrivateAccess = "true"))
	bool bIsInAir;

	/** Whether or not the character is accelerating */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Movement", meta = (AllowPrivateAccess = "true"))
	bool bIsAccelerating;

	/** Offset Yaw used for strafing */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Movement", meta = (AllowPrivateAccess = "true"))
	float MovementOffsetYaw;

	/** Offset Yaw the frame before we stopped moving */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Movement", meta = (AllowPrivateAccess = "true"))
	float LastMovementOffsetYaw{0.f};

	// private methods
	//////
};
