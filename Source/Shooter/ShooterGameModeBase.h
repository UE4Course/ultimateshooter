// (c) 2022 Laurent Ongaro - Gameamea Studio

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"

#include "ShooterGameModeBase.generated.h"

/**
 *
 */
UCLASS()
class SHOOTER_API AShooterGameModeBase : public AGameModeBase {
	GENERATED_BODY()
};
