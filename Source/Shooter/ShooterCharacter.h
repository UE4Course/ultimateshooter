// (c) 2022 Laurent Ongaro - Gameamea Studio

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"

#include "ShooterCharacter.generated.h"

class USoundCue;
UCLASS()
class SHOOTER_API AShooterCharacter : public ACharacter {
	GENERATED_BODY()

public:
	// public properties
	//////

	// public methods
	//////

	/** Get the Get the Camera Boom component */
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Get the Get the follow Camera component */
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

	/**
	 * Sets default values for this character's properties
	 */
	AShooterCharacter();

	// public overriden methods
	//////

	/** Sets default values for this character's properties */
	virtual void Tick(float DeltaTime) override;

	/** Called to bind functionality to input */
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

protected:
	// protected properties
	//////

	// protected methods
	//////

	/** Called when the game starts or when spawned */
	virtual void BeginPlay() override;

	/** Called for forward/backward input */
	void MoveForward(float Value);

	/** Called for left/right input */
	void MoveRight(float Value);

	/**
	 * Called by input to turn at a given rate
	 * @param Rate: Normalized rate, ie 1.0 means 100% of the desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called by input to look up/down at a given rate
	 * @param Rate: Normalized rate, ie 1.0 means 100% of the desired turn rate
	 */
	void LookUpAtRate(float Rate);

	/** Called when the fire button is pressed */
	void FireWeapon();

	bool GetBeamEndLocation(const FVector& MuzzleSocketLocation, FVector& OutBeamLocation) const;

private:
	// private properties
	//////

	/** camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera", meta = (AllowPrivateAccess = "true"))
	USpringArmComponent* CameraBoom;

	/** camera that follows the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera", meta = (AllowPrivateAccess = "true"))
	UCameraComponent* FollowCamera;

	/** Base turn rate, in deg/s */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera", meta = (AllowPrivateAccess = "true"))
	float BaseTurnRate;

	/** Base look up/down rate, in deg/s */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera", meta = (AllowPrivateAccess = "true"))
	float BaseLookupRate;

	/** Crosshairs offset useful to avoid duplicate value between C++ and BP */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera", meta = (AllowPrivateAccess = "true"))
	FVector2D CrosshairsOffset{FVector2D(0.f, -50.f)};

	/**Randomized gun shot sound cue */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Combat", meta = (AllowPrivateAccess = "true"))
	USoundCue* FireSound;

	/** Flash Spawned at BarrelSocket */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Combat", meta = (AllowPrivateAccess = "true"))
	UParticleSystem* MuzzleFlash;

	/** Montage for firing a weapon */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Combat", meta = (AllowPrivateAccess = "true"))
	UAnimMontage* HipFireMontage;

	/** Particles Spawned at when a bullet hits something */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Combat", meta = (AllowPrivateAccess = "true"))
	UParticleSystem* ImpactParticles;

	/** Smoke Trail for Bullets */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Combat", meta = (AllowPrivateAccess = "true"))
	UParticleSystem* BeamParticles;

	// private methods
	//////
};
