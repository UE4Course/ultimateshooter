// (c) 2022 Laurent Ongaro - Gameamea Studio

#include "ShooterAnimInstance.h"

#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "ShooterCharacter.h"

void UShooterAnimInstance::UpdateAnimationProperties(const float DeltaTime) {
	if (ShooterCharacter != nullptr) {
		// if not already done, try to get the character reference in each anim frame
		ShooterCharacter = Cast< AShooterCharacter >(TryGetPawnOwner());
	}
	if (ShooterCharacter) {
		FVector Velocity = ShooterCharacter->GetVelocity();
		Velocity.Z = 0;
		Speed = Velocity.Size();
		bIsInAir = ShooterCharacter->GetCharacterMovement()->IsFalling();
		bIsAccelerating = (ShooterCharacter->GetCharacterMovement()->GetCurrentAcceleration().Size() > 0.f);

		const FRotator AimRotation = ShooterCharacter->GetBaseAimRotation();
		const FRotator MovementRotation = UKismetMathLibrary::MakeRotFromX(ShooterCharacter->GetVelocity());
		MovementOffsetYaw = UKismetMathLibrary::NormalizedDeltaRotator(MovementRotation, AimRotation).Yaw;
		/*
		if (GEngine) {
		    FString const DebugAimRotationMessage = FString::Printf(TEXT("Base Aim rotation: %f"), AimRotation.Yaw);
		    GEngine->AddOnScreenDebugMessage(1, 0.f, FColor::White, DebugAimRotationMessage);
		    FString const DebugMovementRotationMessage = FString::Printf(TEXT("Movement rotation: %f"), MovementRotation.Yaw);
		    GEngine->AddOnScreenDebugMessage(1, 0.f, FColor::White, DebugMovementRotationMessage);
		  FString const DebugMovementOffsetMessage = FString::Printf(TEXT("Movement offset Yaw: %0.2f"), MovementOffsetYaw);
		  GEngine->AddOnScreenDebugMessage(1, 0.f, FColor::White, DebugMovementOffsetMessage);
		}
		*/
		if (Speed) {
			LastMovementOffsetYaw = MovementOffsetYaw;
		}
	}
}

void UShooterAnimInstance::NativeInitializeAnimation() {
	ShooterCharacter = Cast< AShooterCharacter >(TryGetPawnOwner());
}
