// (c) 2022 Laurent Ongaro - Gameamea Studio

#include "ShooterCharacter.h"

#include "Camera/CameraComponent.h"
#include "Engine/SkeletalMeshSocket.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"
#include "Sound/SoundCue.h"

// replace magic numbers by constants
const FVector SocketOffset{FVector(0.f, 50.f, 50.f)};
constexpr float TargetArmLength{300.f};
const FRotator RotationRate = FRotator(0.f, 540.f, 0.f);
constexpr float JumpZVelocity = 600.f;
constexpr float AirControl = 0.2f;
constexpr float WeaponRange = 50'000.f;  // add ' in numbers are ignored and improve readability

// Sets default values
AShooterCharacter::AShooterCharacter() : BaseTurnRate(45.f), BaseLookupRate(45.f) {
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Creates a camera boom (pulls towards the character if there is a collision)
	CameraBoom = CreateDefaultSubobject< USpringArmComponent >(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = TargetArmLength;
	CameraBoom->bUsePawnControlRotation = true;  // Rotate the arm based on the controller
	CameraBoom->SocketOffset = SocketOffset;

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject< UCameraComponent >(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	// Do not Rotate the camera relative to the arm
	FollowCamera->bUsePawnControlRotation = false;

	// does not rotate the mesh with the input (only the camera will rotate)
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = true;
	bUseControllerRotationRoll = false;
	// configure character mouvement
	// THIS SETTINGS MUST ALSO BE CHANGED IN THE CHILD BLUEPRINT TO SEE THEM IN ACTION because they are override with their start value by default
	GetCharacterMovement()->bOrientRotationToMovement = false;  // character move in the direction of input
	GetCharacterMovement()->RotationRate = RotationRate;        // ... at this rotation rate
	GetCharacterMovement()->JumpZVelocity = JumpZVelocity;
	GetCharacterMovement()->AirControl = AirControl;

	CrosshairsOffset = FVector2D(0.f, -50.f);
}

// Called when the game starts or when spawned
void AShooterCharacter::BeginPlay() {
	Super::BeginPlay();
}

void AShooterCharacter::Tick(const float DeltaTime) {
	Super::Tick(DeltaTime);
}

void AShooterCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) {
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	check(PlayerInputComponent);
	PlayerInputComponent->BindAxis("MoveForward", this, &AShooterCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AShooterCharacter::MoveRight);
	PlayerInputComponent->BindAxis("TurnRate", this, &AShooterCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AShooterCharacter::LookUpAtRate);

	// Direct call to the parent class function cause no need to scale or change input values
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	// Direct call to the parent class function cause no need to scale or change input values
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);

	// Direct call to the parent class function cause no need to scale or change input values
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	// Direct call to the parent class function cause no need to scale or change input values
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAction("FireButton", IE_Pressed, this, &AShooterCharacter::FireWeapon);
}

void AShooterCharacter::MoveForward(const float Value) {
	if (Controller != nullptr && Value != 0) {
		// Find out which way is forward Direction
		const FRotator Rotation{Controller->GetControlRotation()};
		const FRotator YawRotation{0, Rotation.Yaw, 0};
		const FVector Direction{FRotationMatrix{YawRotation}.GetUnitAxis(EAxis::X)};

		AddMovementInput(Direction, Value);
	}
}

void AShooterCharacter::MoveRight(const float Value) {
	if (Controller != nullptr && Value != 0) {
		// Find out which way is right Direction
		const FRotator Rotation{Controller->GetControlRotation()};
		const FRotator YawRotation{Rotation.Pitch, 0, 0};
		const FVector Direction{FRotationMatrix{YawRotation}.GetUnitAxis(EAxis::Y)};

		AddMovementInput(Direction, Value);
	}
}

void AShooterCharacter::TurnAtRate(const float Rate) {
	if (Controller != nullptr && Rate != 0) {
		AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
	}
}

void AShooterCharacter::LookUpAtRate(const float Rate) {
	if (Controller != nullptr && Rate != 0) {
		AddControllerPitchInput(Rate * BaseLookupRate * GetWorld()->GetDeltaSeconds());
	}
}

// ReSharper disable once CppMemberFunctionMayBeConst
// !! NO CONST METHOD FOR DELEGATE
void AShooterCharacter::FireWeapon() {
	// UE_LOG(LogTemp, Warning, TEXT("FIRE"));
	if (FireSound) {
		UGameplayStatics::PlaySound2D(this, FireSound);
	}
	if (const USkeletalMeshSocket* BarrelSocket = GetMesh()->GetSocketByName("BarrelSocket")) {
		const FTransform SocketTransform = BarrelSocket->GetSocketTransform(GetMesh());
		if (MuzzleFlash) {
			UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), MuzzleFlash, SocketTransform);
		}

		// ReSharper disable once CppTooWideScopeInitStatement
		FVector BeamEnd;
		if (GetBeamEndLocation(SocketTransform.GetLocation(), BeamEnd)) {
			// spawn impact particles after updating BeamEnd (and if we have an impact)
			if (ImpactParticles) {
				UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ImpactParticles, BeamEnd);
			}

			// spawn Smoke trail for bullet
			if (BeamParticles) {
				if (UParticleSystemComponent* Beam = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), BeamParticles, SocketTransform)) {
					Beam->SetVectorParameter(FName("Target"), BeamEnd);
				}
			}
		}

		// ReSharper disable once CppTooWideScopeInitStatement
		UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
		if (AnimInstance && HipFireMontage) {
			AnimInstance->Montage_Play(HipFireMontage);
			AnimInstance->Montage_JumpToSection(FName("StartFire"));
		}
	}
}

bool AShooterCharacter::GetBeamEndLocation(const FVector& MuzzleSocketLocation, FVector& OutBeamLocation) const {
	// get current size of the viewport
	FVector2d ViewportSize;
	if (GEngine && GEngine->GameViewport) {
		GEngine->GameViewport->GetViewportSize(ViewportSize);
	}
	const FVector2D CrossHairLocation = FVector2D(ViewportSize.X / 2.f, ViewportSize.Y / 2.f) + CrosshairsOffset;
	// get world position and direction of crosshairs
	FVector CrossHairWorldPosition;
	FVector CrossHairWorldDirection;

	// ReSharper disable once CppTooWideScope
	bool bScreenToWorld = UGameplayStatics::DeprojectScreenToWorld(
		UGameplayStatics::GetPlayerController(this, 0), CrossHairLocation, CrossHairWorldPosition, CrossHairWorldDirection);
	// was de-projection successful ?
	if (bScreenToWorld) {
		FHitResult ScreenTraceHit;
		const FVector ScreenTraceStart{CrossHairWorldPosition};
		const FVector ScreenTraceEnd{ScreenTraceStart + CrossHairWorldDirection * WeaponRange};
		OutBeamLocation = ScreenTraceEnd;
		// Trace toward from crosshairs world location
		GetWorld()->LineTraceSingleByChannel(ScreenTraceHit, ScreenTraceStart, ScreenTraceEnd, ECC_Visibility);
		// Was there a trace hit ?
		if (ScreenTraceHit.bBlockingHit) {
			// beam end point is now trace hit location
			OutBeamLocation = ScreenTraceHit.Location;
			// UE_LOG(LogTemp, Warning, TEXT("ScreenTraceHit"));
		}
		// perform a second trace from the gun barrel to see if the bullet is not impacting another object (because the first trace starts FROM THE
		// CROSSHAIRS)
		FHitResult WeaponTraceHit;
		const FVector WeaponTraceStart{MuzzleSocketLocation};
		const FVector WeaponTraceEnd{OutBeamLocation};
		GetWorld()->LineTraceSingleByChannel(WeaponTraceHit, WeaponTraceStart, WeaponTraceEnd, ECC_Visibility);
		// Was an object between the barrel and OutBeamLocation ?
		if (WeaponTraceHit.bBlockingHit) {

			// beam end point is now trace hit location
			OutBeamLocation = WeaponTraceHit.Location;
			// UE_LOG(LogTemp, Warning, TEXT("WeaponTraceHit"));
		}

		return (ScreenTraceHit.bBlockingHit || WeaponTraceHit.bBlockingHit);
	}
	return false;
}