// (c) 2022 Laurent Ongaro - Gameamea Studio

#include "Shooter.h"

#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE(FDefaultGameModuleImpl, Shooter, "Shooter");
