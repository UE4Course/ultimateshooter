// (c) 2022 Laurent Ongaro - Gameamea Studio

using UnrealBuildTool;
using System.Collections.Generic;

public class ShooterEditorTarget : TargetRules {
	public ShooterEditorTarget(TargetInfo Target) : base(Target) {
		Type = TargetType.Editor;
		DefaultBuildSettings = BuildSettingsVersion.V2;
		ExtraModuleNames.AddRange(new string[] { "Shooter" });
	}
}