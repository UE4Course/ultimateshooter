# UltimateShooter

## Description

Un jeu de type FPS inspiré de Hunger-Games avec des larges terrains,une AI avancée, gestion de réseau basique, collecte d'objets, animations...

## Objectifs

TODO

## Mouvements et actions du joueur

Quitter le jeu: touche ESC suivi de clic gauche sur la croix, ou clic gauche sur la fenêtre suivi des touches alt+F4.

TODO

## Interactions

TODO

## Copyrights

Tiré du tutoriel "Unreal Engine C++ The Ultimate Shooter Course", disponible sur le site https://www.udemy.com/course/unreal-engine-the-ultimate-shooter-course

------------------
(C) 2022 GameaMea (http://www.gameamea.com)

==================================================================================================

## Description

A FPS game inspired by Hunger-Games with large terrains, advanced AI, basic networking, collecting objects, animations...

## Targets

TODO

## Movements and actions of the player

Quit the game: ESC key followed by left clicking on the cross, or left click on the window followed by alt + F4.

TODO

## Interactions

TODO

## Copyrights

Taken from the tutorial "Unreal Engine C++ The Ultimate Shooter Course", available at https://www.udemy.com/course/unreal-engine-the-ultimate-shooter-course

------------------
(C) 2022 GameaMea (http://www.gameamea.com)
