# Help Documentation

## commands

### WINDOWS

- command for building the project the first time (run by editor)
`
R:/Epic Games/UE_4.27/Engine/Binaries/DotNET/UnrealBuildTool.exe Development Win64 -Project="R:/Projets/UE4/Udemy/Shooter/Shooter.uproject" -TargetType=Editor -Progress -NoEngineChanges -NoHotReloadFromIDE
`
- command for building the project the first time (run by Rider)
`
R:\Epic Games\UE_4.27\Engine\Binaries\DotNET\UnrealBuildTool.exe UE4Editor Win64 Development -Project=T:\Temp\laurent\UnrealLink\Legujyk\HostProject\HostProject.uproject -plugin=T:\Temp\laurent\UnrealLink\Legujyk\HostProject\Plugins\RiderLink\RiderLink.uplugin -iwyu -noubtmakefiles -manifest=T:\Temp\laurent\UnrealLink\Legujyk\HostProject\Saved\Manifest-UE4Editor-Win64-Development.xml -nohotreload -log="C:\Users\laure\AppData\Roaming\Unreal Engine\AutomationTool\Logs\R+Epic+Games+UE_4.27\UBT-UE4Editor-Win64-Development.txt"
`
